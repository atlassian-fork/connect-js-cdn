/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                             *
 *   (                                                                                         *
 *   )\ )                                       )       (                                      *
 *  (()/(     (         (      (          )  ( /(   (   )\ )                                   *
 *   /(_))   ))\ `  )   )(    ))\  (   ( /(  )\()) ))\ (()/(                                   *
 *  (_))_   /((_)/(/(  (()\  /((_) )\  )(_))(_))/ /((_) ((_))                                  *
 *   |   \ (_)) ((_)_\  ((_)(_))  ((_)((_)_ | |_ (_))   _| |                                   *
 *   | |) |/ -_)| '_ \)| '_|/ -_)/ _| / _` ||  _|/ -_)/ _` |                                   *
 *   |___/ \___|| .__/ |_|  \___|\__| \__,_| \__|\___|\__,_|                                   *
 *              |_|                                                                            *
 *                                                                                             *
 *  The code added to iframe.js coming from simple-xdm is for backwards compatibility only     *
 *  No new code may be added to plugin/* All modules must be defined in the host product       *
 *  For examples on how to extend ACJS please see https://bitbucket.org/atlassian/simple-xdm   *
 *                                                                                             *
 *        " No changes necessary in the iframe library when the host API changes "             *
 *                                                                                             *
 *  also see https://bitbucket.org/atlassian/atlassian-connect-js and host/*                   *
 *                                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

if(AP._hostModules.confluence){
  var original_onMacroPropertyPanelEvent = AP._hostModules.confluence.onMacroPropertyPanelEvent.bind({});
  AP.confluence.onMacroPropertyPanelEvent = AP._hostModules.confluence.onMacroPropertyPanelEvent = function(eventBindings) {
    AP.register(eventBindings);
    original_onMacroPropertyPanelEvent();
  };
}