describe('Confluence', function(){
  var onMacroPropertyPanelEventSpy;
  beforeAll(function(done){
    if (!window.AP) {
      window.AP = {
        '_hostModules': {},
        register: jasmine.createSpy('spy')
      };
    }
    onMacroPropertyPanelEventSpy = jasmine.createSpy('onMacroPropertyPanelEvent');
    window.AP._hostModules.confluence = window.AP.confluence = {
      onMacroPropertyPanelEvent: onMacroPropertyPanelEventSpy
    };
    require(['confluence'], function(p2compat){
        done();
    });

  });

  it('extends the existing AP.confluence api', function(){
    expect(jasmine.isSpy(window.AP.confluence.onMacroPropertyPanelEvent)).toBe(false);
  });

  describe('onMacroPropertyPanelEventSpy binding', function() {
    it('calls original onMacroPropertyPanelEventSpy', function() {
      window.AP.confluence.onMacroPropertyPanelEvent('test');
      expect(onMacroPropertyPanelEventSpy.calls.count()).toEqual(1);
    });
  });
});